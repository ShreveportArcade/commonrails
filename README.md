# Dokku Setup
You'll need to replace "appName", "dbName", "redisName", and the config vars below accordingly.

```
dokku apps:create appName

dokku postgres:create dbName

dokku postgres:link dbName appName

dokku redis:create redisName

dokku redis:link redisName appName

dokku config:set appName AWS_ACCESS_KEY_ID=awsid1234 AWS_SECRET_ACCESS_KEY=awskey1234 CLOUDFRONT_URL=url1234.cloudfront.net FOG_DIRECTORY=directoryName FOG_PROVIDER=AWS GOOGLE_API_KEY=asdf1234

dokku letsencrypt appName
```

If you haven't already done so, you'll also need to install the following dokku plugin:
 - https://github.com/F4-Group/dokku-apt

# Local Setup
```
git clone git@bitbucket.org:ShreveportArcade/commonrails.git

bundle install

bundle exec rake db:setup

rails s
```

# Initial Deploy
```
git remote add dokku dokku@server.url:appName

git push dokku master

ssh -t dokku@server.url run appName bundle exec rake db:setup
```

# Depoly Changes
For each subsequent deployment, simply commit your changes and push to dokku.

```
git push dokku master
```

If you made any changes to the schema, you will also need to migrate the database.

```
ssh -t dokku@server.url run appName bundle exec rake db:migrate
```


