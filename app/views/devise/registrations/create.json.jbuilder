json.user do
  json.authentication_token current_user.authentication_token
  
  json.partial! "#{current_version}/users/user", user: current_user
end