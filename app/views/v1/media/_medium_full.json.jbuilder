json.title medium.title
json.creator medium.creator
json.text medium.text
json.headerURL medium.header_url
json.videoURL medium.video_url

if medium.image?
  json.lowResImageURL medium.image.url(:low)
  json.highResImageURL medium.image.url(:high)
end

if medium.document?
  json.documentURL medium.document.url
end

if medium.audio?
  json.audioURL medium.audio.url(:desktop)
  json.mobileAudioURL medium.audio.url(:mobile)
  json.webAudioURL medium.audio.url(:web)
end