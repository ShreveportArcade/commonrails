json.media do
  json.array! @media, partial: "#{current_version}/media/medium_preview", as: :medium
end