json.id medium.id
json.title medium.title
json.creator medium.creator
json.category medium.category
json.textPreview medium.text_preview
json.thumbnailURL medium.thumbnail_url
