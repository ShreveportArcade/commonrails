json.partial! "#{current_version}/media/medium_full", medium: @medium

json.links do
  json.array! @medium.links, partial: "#{current_version}/links/link", as: :link
end