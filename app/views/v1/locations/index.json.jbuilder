json.locations do
  json.array! @locations, partial: "#{current_version}/locations/location", as: :location
end