class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :null_session, if: ->(controller) { controller.request.format == "application/json" }

  before_action :authenticate_user_from_token!
  before_action :authenticate_user!

  before_filter :set_paper_trail_whodunnit

  rescue_from(CanCan::AccessDenied) do |exception|
    render_error :unauthorized, "Unauthorized to perform this action."
  end

  rescue_from(ActiveRecord::RecordNotFound)  do |exception|
    render_error :bad_request, "Record was not found."
  end

  rescue_from(ActionController::ParameterMissing) do |exception|
    render_error :bad_request, "Required parameter, \"#{exception.param}\", was not sent."
  end

  rescue_from(ActionDispatch::ParamsParser::ParseError) do |exception|
    render_error :bad_request, "Invalid JSON body sent to server."
  end

  if !Rails.env.development?
    rescue_from(Exception) do |exception|
      render_error :bad_request, exception.message
    end
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end
  
  protected

  def render_error(status, message)
    store_location

    flash[:alert] = message

    respond_to do |format|
      format.html { redirect_to main_app.root_url }
      format.json { render json: { error: message }, status: status }
    end
  end

  def render_object_errors(object)
    logger = Logger.new("log/error_messages.log")
    logger.level = Logger::DEBUG
    logger.debug "#{object.errors.full_messages}"
    render json: { error: object.errors.full_messages }
  end

  private

  def store_location
    session[:return_to] = request.base_url + request.path
  end

  def authenticate_user_from_token!
    user_token = params[:authentication_token].presence
    user = user_token && User.find_by_authentication_token(user_token)

    if user
      sign_in user, store: false
    end
  end
  
end