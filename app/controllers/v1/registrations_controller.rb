class V1::RegistrationsController < Devise::RegistrationsController

  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_filter :verify_authenticity_token
  skip_before_filter :authenticate_user!

  def create
    respond_to do |format|
      format.html {
        super
      }
      format.json {
        build_resource(sign_up_params)
        
        if resource.valid?
          resource.update_attribute(:device_token, params[:device_token])

          # HACK: current_user saved in session requiring signout
          sign_out(current_user)
          sign_up(resource_name, resource)
        else
          render_object_errors(resource)
        end
      }
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).concat([:first_name, :last_name])
  end
end