class V1::LocationsController < V1::ApiController
  
  skip_before_action :authenticate_user_from_token!, only: [:index]
  skip_before_action :authenticate_user!, only: [:index]

  def index
    @locations = Location.all
  end

end