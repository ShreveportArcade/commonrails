class V1::MediaController < V1::ApiController
  
  skip_before_action :authenticate_user_from_token!, only: [:index, :show]
  skip_before_action :authenticate_user!, only: [:index, :show]

  def index
    @media = Location.find(params[:location_id]).media.order(:display_priority)
  end

  def show
    @medium = Medium.find(params[:id])
  end

end