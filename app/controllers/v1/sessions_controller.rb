class V1::SessionsController < Devise::SessionsController

  skip_before_filter  :verify_authenticity_token

  def create
    respond_to do |format|
      format.html {
        super
      }
      format.json {
        self.resource = warden.authenticate!(auth_options)
        self.resource.update_attribute(:device_token, params[:device_token])
        sign_in(resource_name, resource)
      }
    end
  end

  def destroy
    current_user.reset_authentication_token! if current_user

    respond_to do |format|
      format.html {
        super
      }
      format.json {
        head :no_content
      }
    end
  end

end