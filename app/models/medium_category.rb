class MediumCategory < ActiveRecord::Base
  
  has_many :media

  has_paper_trail

end
