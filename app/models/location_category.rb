class LocationCategory < ActiveRecord::Base
  
  has_many :locations

  has_paper_trail

end
