class Ability
  if defined?(CanCan)
    include CanCan::Ability
  end

  def initialize(user)
    
    user ||= User.new
    
    if user.role?('Admin')
      can :manage, :all
    elsif user.role?('Manage')
      can :access, :rails_admin
      can :dashboard           
      can :manage, [Medium, Location, Link]
      can :read, [LocationCategory, MediumCategory]
      can :manage, User, roles: {name: ['Manage', 'Enter']}
      can :manage, Role, name: ['Manage', 'Enter']
    elsif user.role?('Enter')
      can :access, :rails_admin
      can :dashboard           
      can :manage, [Medium, Location, Link]
      can :read, [LocationCategory, MediumCategory]
    end
  end
end
