class Medium < ActiveRecord::Base

  RailsAdmin.config do |config|
    config.model Medium do
      edit do
        field :display_priority do
          help "This controls which order things appear in the list within the app. Smaller numbers appear first. Default is 100."
        end
        field :title do
          help "Large text displayed to the right of the image in the list view as well as on the bottom right corner of the image on the detail view."
        end
        field :creator do
          help "Small text displayed below the title in both the list view and detail view."
        end
        field :text do
          help "Text that appears below the image in the detail view. The first 200 characters will appear below the title and creator in the list view."
        end
        field :image do
          help "This image will be processed into 4 sizes - the 240x135 thumbnail image that appears in the list view, the 960x540 header that appears on the detail view, a scaled low resolution image (max 256x256) that loads first when viewing the image fullscreen, and a scaled high resolution image (max 2048x2048) that loads second when viewing fullscreen."
          delete_method :delete_image
        end
        field :audio do
          help "This audio file will be process into 3 different formats - an ogg that can be played on desktops, an MP3 that can be played on mobile devices, and an AAC that can be played in a web browser."
          delete_method :delete_audio
        end
        field :document do
          help "This document must be a PDF. If the image file above isn't present, the first page of this PDF will be used instead."
          delete_method :delete_document
        end
        field :video_url do
          label "Video URL"
          help "If this URL is present, a large play button will appear over the image in the detail view. Touching the button will take you out of the app and open the URL in your device's browser. If the URL is a YouTube link and the image above isn't present, the video thumbnail will be used instead."
        end
        field :medium_category do
          label "Category"
          help "This will determine which category the medium will appear under in the app. If none is selected, it will default to 'Images'."
        end
        field :links do
          help "If any links are present, a section titled 'Additional Links' will appear at the bottom of the detail view."
        end
        field :locations do
          help "This medium must have at least one location to appear in the app."
        end
      end
    end
  end

  has_and_belongs_to_many :links
  has_and_belongs_to_many :locations
  belongs_to :medium_category

  attr_accessor :delete_image, :delete_audio, :delete_document

  has_attached_file :image, PAPERCLIP_OPTIONS.merge(
    styles: {
      thumb: ["240x135#", :jpg],
      header: ["960x540#", :jpg],
      low: ["256x256>", :jpg],
      high: ["2048x2048>", :jpg]
    }, 
    only_process: [:thumb, :low]
  ) 
  
  has_attached_file :audio, PAPERCLIP_OPTIONS.merge(
    styles: {
      desktop: { format: "ogg" },
      mobile: { format: "mp3", 
        convert_options: { 
          output: { ar: 44100, "qscale:a" => 8 } 
        } 
      },
      web: { format: "aac" }
    }, 
    processors: [:transcoder]
  )
  
  has_attached_file :document, PAPERCLIP_OPTIONS.merge(
    styles: {
      thumb: ["240x135#", :jpg], 
      header: ["960x540#", :jpg]
    }, 
    only_process: [:thumb]
  )

  before_validation { image.clear if delete_image == '1' }
  before_validation { audio.clear if delete_audio == '1' }
  before_validation { document.clear if delete_document == '1' }

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  validates_attachment_content_type :audio, content_type: /\Aaudio\/.*\Z/
  validates_attachment_content_type :document, content_type: "application/pdf"
  
  process_in_background :image, only_process: [:header, :high]
  process_in_background :audio, only_process: [:desktop, :mobile, :web]
  process_in_background :document, only_process: [:header]

  has_paper_trail

  def text_preview
    (text && text.length > 200) ? "#{text[0...200]}..." : text
  end

  def header_url
    if image? 
      image.url(:header)
    elsif document?
      document.url(:header)
    elsif video_url.present?
      "http://img.youtube.com/vi/#{youtube_id}/hqdefault.jpg"
    else
      nil
    end
  end

  def thumbnail_url
    if image? 
      image.url(:thumb)
    elsif document?
      document.url(:thumb)
    elsif video_url.present?
      "http://img.youtube.com/vi/#{youtube_id}/mqdefault.jpg"
    else
      nil
    end
  end

  def youtube_id
    match = video_url.match(/.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/)
    if match.present?
      match[1]
    else
      nil
    end
  end

  def category
    if medium_category.present?
      medium_category.name
    else
      "Images"
    end
  end
end
