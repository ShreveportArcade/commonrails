class Location < ActiveRecord::Base
  
  RailsAdmin.config do |config|
    config.model Location do
      edit do
        field :title do
          help "Text displayed on the map above the buildings. When tapped, this text also appears in large font to the right of the category buttons."
        end
        field :info do
          help "Text displayed in a smaller font below the title to the left of the category buttons."
        end
        field :display_height do
          label "Title Height"
          help "Distance in meters the Title will appear above the ground on the map."
        end
        field :display_size do
          label "Title Size"
          help "Size multiplier for Title as it appears on the map. Default is 1. 0.5 will show the title at half the normal size. 2 will show it at double the normal size."
        end
        field :min_height do
          label "Minimum Camera Height"
          help "If the camera is below this height (in meters), this location will no longer show up on the map. This is useful for locations with sub-locations. (ie. 800 Block disappears at 500 feet, and the buildings making up that block are displayed instead.)"
        end
        field :max_height do
          label "Maximum Camera Height"
          help "If the camera is above this height (in meters), this location will not show up on the map. At 1000, the location will show up on app load. It is suggested to use a number no higher than 800, so nothing shows up until after the title has faded out."
        end
        field :has_dist_alerts, :boolean do
          label "Enable Distance Based Alerts"
          help "If enabled, this location will appear in the nearby notification bar."
        end
        field :max_alert_dist do
          label "Maximum Alert Distance"
          help "Max distance in meters this location will appear in the nearby notification bar."
        end
        field :latitude, :map do
          label "Latitude / Longitude"
          help "Click on the spot on the map you want this Location to appear at."
          default_latitude 32.509130 
          default_longitude -93.750950
          default_width 400
          default_height 400
          default_zoom_level 18
          google_api_key ENV['GOOGLE_API_KEY']
        end
        field :media do
          help "Locations without media will still appear on the map, but the media list will be empty when the location is selected."
        end
      end
    end
  end

  belongs_to :location_category
  has_and_belongs_to_many :media
  has_paper_trail

end

