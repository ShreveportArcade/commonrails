class User < ActiveRecord::Base

  RailsAdmin.config do |config|
    config.model User do
      edit do
        field :name
        field :email
        field :password
        field :password_confirmation
        field :roles
      end
      list do
        field :name
        field :email
        field :roles
      end
    end 
  end

  devise :database_authenticatable, :omniauthable, :recoverable, :registerable, 
         :rememberable, :trackable, :validatable

  has_many :authentications, dependent: :destroy
  has_and_belongs_to_many :roles

  before_save :ensure_authentication_token

  scope :with_social_uids, ->(provider, uids) { joins(:authentications).where(authentications: { provider: provider, uid: uids }) }
  scope :who_arent, ->(user) { where.not(id: user.id) }
  scoped_search on: [:first_name, :last_name, :email]

  has_paper_trail

  def after_token_authentication
    reset_authentication_token!
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def reset_authentication_token!
    self.authentication_token = generate_authentication_token
    save
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).count > 0
    end
  end

  def role?(role)
    self.roles.map { |r| r.name.to_sym.downcase }.include? role.to_sym.downcase
  end

  def admin?
    role?(:admin) || role?(:manage)
  end

  protected

  def email_required?
    true
  end

  private
end