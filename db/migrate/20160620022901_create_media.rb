class CreateMedia < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :title
      t.string :url
    end

    create_table :media do |t|
      t.string :title
      t.string :category
      t.string :creator
      t.text :text
      t.attachment :image
      t.attachment :audio
      t.string :video_url
    end

    create_table :links_media, id: false do |t|
      t.references :link, :medium
    end
  end
end
