class AddDocumentToMedim < ActiveRecord::Migration
  def change
    add_attachment :media, :document
  end
end
