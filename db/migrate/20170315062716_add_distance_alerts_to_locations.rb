class AddDistanceAlertsToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :has_dist_alerts, :bool, default: true
    add_column :locations, :max_alert_dist, :float, default: 30
  end
end
