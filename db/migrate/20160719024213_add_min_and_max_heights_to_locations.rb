class AddMinAndMaxHeightsToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :min_height, :float, default: 0
    add_column :locations, :max_height, :float, default: 1000
    remove_column :locations, :zoom_level, :integer
  end
end
