class AddDisplayPriorityToMedia < ActiveRecord::Migration
  def change
    add_column :media, :display_priority, :integer, default: 100
  end
end
