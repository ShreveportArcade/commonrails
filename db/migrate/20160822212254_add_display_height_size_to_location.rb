class AddDisplayHeightSizeToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :display_height, :float, default: 25
    add_column :locations, :display_size, :float, default: 1
  end
end
