class AddZoomLevelToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :zoom_level, :integer, default: 0
  end
end
