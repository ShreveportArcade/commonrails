class AddProcessingToMedia < ActiveRecord::Migration
  def change
    add_column :media, :image_processing, :boolean
    add_column :media, :audio_processing, :boolean
    add_column :media, :document_processing, :boolean
  end
end
