class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :title
      t.string :info
      t.string :category
      t.float :latitude
      t.float :longitude
    end

    create_table :locations_media, id: false do |t|
      t.references :location, :medium
    end
  end
end
