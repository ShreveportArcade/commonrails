class CreateMediumCategories < ActiveRecord::Migration
  def change
    remove_column :media, :category, :string

    create_table :medium_categories do |t|
      t.string :name
    end

    add_reference :media, :medium_category, index: true, foreign_key: true
  end
end
