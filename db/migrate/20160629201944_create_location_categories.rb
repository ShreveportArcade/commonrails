class CreateLocationCategories < ActiveRecord::Migration
  def change
    remove_column :locations, :category, :string

    create_table :location_categories do |t|
      t.string :name
    end

    add_reference :locations, :location_category, index: true, foreign_key: true
  end
end
