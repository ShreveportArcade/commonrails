puts "Seeding Roles:"
admin_role = Role.where(name: 'Admin').first_or_create; print(".")
manage_role = Role.where(name: 'Manage').first_or_create; print(".")
enter_role = Role.where(name: 'Enter').first_or_create; print(".")
puts

puts "Seeding Users:"
user = User.where(email: 'admin@shreveportcommon.com').first_or_create
user.password = "password"
user.password_confirmation = "password"
user.roles << admin_role unless user.roles.include? admin_role
user.save!; print(".")
puts

puts "Seeding Location Categories:"
LocationCategory.where(name: 'building').first_or_create; print(".")
LocationCategory.where(name: 'info').first_or_create; print(".")
puts

puts "Seeding Medium Categories:"
MediumCategory.where(name: 'Tours').first_or_create; print(".")
MediumCategory.where(name: 'Music').first_or_create; print(".")
MediumCategory.where(name: 'Stories').first_or_create; print(".")
MediumCategory.where(name: 'Images').first_or_create; print(".")
MediumCategory.where(name: 'History').first_or_create; print(".")
puts