RailsAdmin.config do |config|
  config.main_app_name = ['Common Link', 'Admin']
  config.current_user_method { current_user }
  config.excluded_models = ['Authentication', 'Role', 'PaperTrail::Version', 'PaperTrail::VersionAssociation']
  config.authorize_with :cancan
  config.audit_with :paper_trail, 'User', 'PaperTrail::Version'
end
