Paperclip.options[:log] = false
Paperclip.options[:command_path] = if Rails.env.dev?
  "/usr/local/bin"
else
  "/usr/bin"
end

PAPERCLIP_OPTIONS = {
  hash_secret: Rails.application.secrets.secret_key_base,
  hash_data: ":class/:attachment/:id/:style",
  default_url: ->(attachment) { attachment.send(:interpolate, "missing/:class_:attachment.png") },
  use_timestamp: false
}

PAPERCLIP_STORAGE_OPTIONS = if Rails.env.staging? || Rails.env.production?
  name = CommonLink::Application.name.downcase.gsub(/ /, "_")
  {
    storage: :s3,
    s3_credentials: {
      access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      bucket: ENV['FOG_DIRECTORY'],
    },
    s3_protocol: "",
    s3_host_alias: ENV["CLOUDFRONT_URL"], 
    path: "#{name}/:class/:attachment/:id_partition/:style/:hash.:extension"
  }
else
  {
    path: ":rails_root/public/system/:class/:attachment/:id_partition/:style/:hash.:extension",
    url: "/system/:class/:attachment/:id_partition/:style/:hash.:extension"
  }
end

PAPERCLIP_OPTIONS.merge!(PAPERCLIP_STORAGE_OPTIONS)
