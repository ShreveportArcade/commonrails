require File.expand_path('../boot', __FILE__)

require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require "rack/cors"

Bundler.require(*Rails.groups)

module CommonLink
  class Application < Rails::Application
    def self.name
      "CommonLink"
    end

    def self.display_name
      "Shreveport Common Link"
    end

    require "#{config.root}/lib/possessive"

    config.paths["config/routes.rb"].concat Dir[Rails.root.join("config/routes/*.rb")]
    config.autoload_paths += %W(#{config.root}/lib)

    config.time_zone = 'Central Time (US & Canada)'

    config.generators do |g|
      g.integration_tool false
      g.performance_tool false
      g.helper false
      g.test_framework :rspec,
        view_specs: false,
        routing_specs: false
    end

    config.time_zone = 'Central Time (US & Canada)'

    config.active_job.queue_adapter = :sidekiq

    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end
  end
end
